﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.IO;

namespace dlltesting
{
    class Program
    {
        [DllImport("2ktexlib.dll",CallingConvention=CallingConvention.Cdecl)]
        public static extern int get_mipmap_size(int w, int h, float mode, int mipmapcount);
        [DllImport("2ktexlib.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void unswizzler(byte[] input_data ,int w, int h, float mode, int mipmapcount, byte[] output_data);
        [DllImport("2ktexlib.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int testing();
        static void Main(string[] args)
        {
            Console.WriteLine("This is a C# program");
            int size= get_mipmap_size(512 , 512 , (float) 0.5 , 1);
            // FileStream fs = File.Open("some.dds",FileMode.Open);
            FileStream wf = File.Open("converted_data",FileMode.Create);
            Byte[] input_data = File.ReadAllBytes("dxt1.dds");
            int image_data_count=input_data.Length-0x80;
            Byte[] image_data= new Byte[image_data_count];
            Byte[] output_data = new Byte[image_data_count];

            Array.Copy(input_data,0x80,image_data,0,image_data_count);

            unswizzler(image_data, 512, 512, (float) 0.5, 10, output_data);
            wf.Write(output_data,0,output_data.Length);
            wf.Close();

        }
    }
}
